package me.themgrf.testmod.items;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.sound.SoundEvents;
import net.minecraft.util.Hand;
import net.minecraft.util.TypedActionResult;
import net.minecraft.world.World;

public class Ambrosium extends Item {

    public Ambrosium(Settings item$Settings_1) {
        super(item$Settings_1);
    }

    @Override
    public TypedActionResult<ItemStack> use(World world, PlayerEntity playerEntity, Hand hand) {
        if (playerEntity.getHealth() < 20) {
            playerEntity.playSound(SoundEvents.UI_TOAST_OUT, 1, 1);
            playerEntity.playSound(SoundEvents.BLOCK_NOTE_BLOCK_PLING, 1, 2);
            playerEntity.setHealth(playerEntity.getHealth() + 2);
            playerEntity.getStackInHand(hand).setCount(playerEntity.getStackInHand(hand).getCount() - 1);
        }
        return super.use(world, playerEntity, hand);
    }
}
