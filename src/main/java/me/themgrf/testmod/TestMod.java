package me.themgrf.testmod;

import me.themgrf.testmod.blocks.AmbrosiumBlock;
import me.themgrf.testmod.items.Ambrosium;
import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.client.itemgroup.FabricItemGroupBuilder;
import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

public class TestMod implements ModInitializer {

    private static String IDENTIFIER = "testmod";

    private static final ItemGroup ITEM_GROUP = FabricItemGroupBuilder.build(new Identifier(IDENTIFIER, "testmod"), () -> new ItemStack(Blocks.COBBLESTONE));

    // Items
    private static final Item AMBROSIUM_ITEM = new Ambrosium(new Item.Settings().group(ITEM_GROUP));

    // Blocks
    private static final Block AMBROSIUM_BLOCK = new AmbrosiumBlock();

    @Override
    public void onInitialize() {
        // Items
        Registry.register(Registry.ITEM, new Identifier(IDENTIFIER, "ambrosium_item"), AMBROSIUM_ITEM);

        // Blocks
        Registry.register(Registry.BLOCK, new Identifier(IDENTIFIER, "ambrosium_block"), AMBROSIUM_BLOCK);
        Registry.register(Registry.ITEM, new Identifier(IDENTIFIER, "ambrosium_block"), new BlockItem(AMBROSIUM_BLOCK, new Item.Settings().group(ITEM_GROUP)));

        System.out.println("Hello Fabric world!");
    }
}
